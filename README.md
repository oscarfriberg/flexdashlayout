# AngNode

### Git-commands

    * git add -A 
    * git commit -m "text" 
    * git push
    * git remote set-url origin https://oscarfriberg@bitbucket.org/oscarfriberg/flexdash.git


## Initial setup

### How do I get set up? ###

* **Fixa NodeJS (och därmed även npm)**
    * 1.) Ladda ner Node js
    
* **Fixa Mnogodb  community edition**
* 1.) Ladda ner Node Community Server

* **Generera ett Angular4-projekt**
    * 1.) Installera node från deras hemsida
    * 2.) I cmd: 
        * `npm install -g @angular/cli`
        * Navigera dit du vill att projektet ska ligga
        * `ng new PROJEKTNAMN`
    * 3.) Öppna i code, och för att köra skriv i term `ng serve` (eller bara i cmd om du är i rätt folder)

* **Installera Exress** *(antingen i cmd i rätt folder, eller VS Code):*
    * `npm install express --save`

* **Installera Mongoose** *(antingen i cmd i rätt folder, eller VS Code):*
    * `npm install --save mongoose`
    * samt ev. `npm install --save mongoose-unique-validator` (endast för att kunna använda unique i SQL-tables?)

* **Starta Node**
    * I cmd:
        * Navigera dit den installerades (om inget angivet prb. C:\Program Files\MongoDB\Server\3.x\bin) 
        * Serversidan: `mongod.exe`
        * Clientsidan: `mongo.exe`

* **Dependencies/libs**
    * Express
    * Mongoose
    * NodeJS
    * Angular4
    * Font-awesome
    * jQuery
    * Bootstrap v4 Beta

* **Database configuration**
    * MongoDB

* **IIS**
    * Installera iisnode från 'https://github.com/Azure/iisnode/downloads'
    * Skapa 'web.config' på server.js/app.js-nivå och pastea:
            `
                <configuration>
                    <system.webServer>
                        <handlers>
                            <add name="iisnode" path="server.js" verb="*" modules="iisnode" />
                        </handlers>
                        <rewrite>
                        <rules>
                            <rule name="sendToNode">
                                <match url="/*"/>
                                <action type="Rewrite" url="server.js"/>
                            </rule>
                        </rules>
                        </rewrite>
                    </system.webServer>
                </configuration>
            `
    * I den mappen som IIS mappas mot:
        * Egenskaper -> Security -> Group or user names: Edit -> Add 'IIS_IUSRS' med fulla rättigheter


* **Deployment instructions**
    * Öppna Source Tree och repositoret
    * "Stage All"
    * Skriv kommentar
    * Commit
    * Push

* **Configre mongoDb as a windows service**
    * Skapa en fil C:\Program Files\MongoDB\Server\3.6\mongod.cfg och i den skriv i systemLog: destination: file path: c:\data\log\mongod.log storage: dbPath: c:\data\db
    * Installera mongo-servicen genom "C:\Program Files\MongoDB\Server\3.6\bin\mongod.exe" --config "C:\Program Files\MongoDB\Server\3.6\mongod.cfg" --install

* **Updates**
    You can upgrade to the latest version of npm using:

    npm install -g npm@latest Or upgrade to the most recent release:

    npm install -g npm@next ng e2e

    Uppdatera anguliar cli

    Uninstall old version If you're using Angular CLI beta.28 or less, you need to uninstall the angular-cli packages:
    npm uninstall -g angular-cli # Remove global package npm uninstall --save-dev angular-cli # Remove from package.json Otherwise, uninstall the @angular/cli packages:

    npm uninstall -g @angular/cli # Remove global package npm uninstall --save-dev @angular/cli # Remove from package.json Also purge the cache and local packages:

    rm -rf node_modules dist # Use rmdir on Windows npm cache clean At this point, you should not have Angular CLI on your system anymore. If invoking Angular CLI at the commandline reveals that it still exists on your system, you will have to manually remove it. See Manually removing residual Angular CLI.

    Update node/npm if necessary Angular CLI now has a minimum requirement of Node 6.9.0 or higher, together with NPM 3 or higher.
    If your Node or NPM versions do not meet these requirements, please refer to the documentation on how to upgrade.

    Install the new version To update Angular CLI to a new version, you must update both the global package and your project's local package:
    npm install -g @angular/cli@latest # Global package npm install --save-dev @angular/cli@latest # Local package npm install # Restore removed dependencies Manually removing residual Angular CLI If you accidentally updated NPM before removing the old Angular CLI, you may find that uninstalling the package using npm uninstall is proving fruitless. This could be because the global install (and uninstall) path changed between versions of npm from /usr/local/lib to /usr/lib, and hence, no longer searches through the old directory. In this case you'll have to remove it manually:

    rm -rf /usr/local/lib/node_modules/@angular/cli

    If the old Angular CLI package still persists, you'll need to research how to remove it before proceeding with the upgrade.


## MongoDB setup och Som Service
    * Om installerades till t.ex. C:\Program Files\MongoDB\Server\3.6\bin; flytta allt som är i 3.6 till en ny folder i C:\MongoDB
    * Skapa en mapp i C:\MongoDB som heter 'logs' och en som heter 'data', där 'data' har en mapp som heter 'db' och en som heter 'log'
    * Skapa en fil som heter mongod.cfg i C:\MongoDB och fyll i: 
        systemLog:
            destination: file
            path: c:\data\log\mongod.log
        storage:
            dbPath: c:\data\db
    * I Cmd: kör commandot: mongod --logpath c:\mongodb\logs\mongo.log --dbpath c:\mongodb\data\db --install


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
