// GENERAL STUFF
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// GRAPHICS & Bootstrap
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsModalModule } from 'ng2-bs3-modal';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

/* Material Design */
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule, MatNativeDateModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule} from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSnackBar } from '@angular/material';
import { MatRadioModule } from '@angular/material/radio';
import { MatIconModule } from '@angular/material/icon';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
// PERFECT SCROLLBAR
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

// SERVICES
import { GetFiltersService } from './services/get-filters/get-filters.service';
import { SetFiltersService } from './services/set-filters/set-filters.service';
import { JiraForestService } from './services/jira-forest/jira-forest.service';
import { WeatherService } from './services/weather/weather.service';
import { AlertService } from './services/alert/alert.service';
import { TwitterService } from './services/twitter/twitter.service';
import { TrainsService } from './services/trains/trains.service';
import { FilterService } from './services/filter/filter.service';
import { SvgInlineConverterService } from './services/svg-inline-converter/svg-inline-converter.service';
import { SharedService } from './services/shared-service/shared-service.service';
import { VariablePasserService } from './services/variable-passer/variable-passer.service';
import { JiraParametersService } from './services/jira-parameters/jira-parameters.service';
import { JiraParamSharedService } from './services/jira-param-shared/jira-param-shared.service';

// COMPONENTS
import { HeaderComponent } from './components/header/header.component';
import { ForestItemComponent } from './components/forest-item/forest-item.component';
import { JiraForestComponent } from './components/jira-forest/jira-forest.component';
import { FlexWrapComponent } from './components/flex-wrap/flex-wrap.component';
import { VoxComponent } from './components/vox/vox.component';
import { TemplateComponent } from './template/template.component';
import { TwitterComponent } from './components/twitter/twitter.component';
import { TrainsComponent } from './components/trains/trains.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { AppComponent } from './components/app/app.component';
import { WeatherComponent } from './components/weather/weather.component';
import { AlertComponent } from './components/alert/alert.component';

// DIRECTIVES
import { LastElementDirective } from './directives/last-element/last-element.directive';

// PIPES
import { TextLengthPipe } from './pipes/text-length/text-length.pipe';
import { DateFormatterPipe } from './pipes/date-formatter/date-formatter.pipe';
import { IssueFilterPipe } from './pipes/issue-filter/issue-filter.pipe';
import { SubStringPipe } from './pipes/sub-string/sub-string.pipe';
import { JiraQuarterPipe } from './pipes/jira-quarter/jira-quarter.pipe';
import { PortfolioFilterPipe } from './pipes/portfolio-filter/portfolio-filter.pipe';
import { ChosenAppsPipe } from './pipes/chosen-apps/chosen-apps.pipe';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { SideMenuSearchComponent } from './components/side-menu-search/side-menu-search.component';
import { SideMenuUserComponent } from './components/side-menu-user/side-menu-user.component';
import { SafePipe } from './pipes/safe/safe.pipe';

import { InlineSVGModule } from 'ng-inline-svg';
import { Safe2Pipe } from './pipes/safe2/safe2.pipe';
import { FilterOutStringPipe } from './pipes/filter-out-string/filter-out-string.pipe';
import { ClickOutsideDirective } from './directives/click-outside/click-outside.directive';
import { ResponsiveModule } from 'ng2-responsive';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    VoxComponent,
    TemplateComponent,
    TwitterComponent,
    TrainsComponent,
    DatePickerComponent,
    AlertComponent,
    TextLengthPipe,
    DateFormatterPipe,
    HeaderComponent,
    ForestItemComponent,
    JiraForestComponent,
    FlexWrapComponent,
    LastElementDirective,
    IssueFilterPipe,
    SubStringPipe,
    JiraQuarterPipe,
    PortfolioFilterPipe,
    ChosenAppsPipe,
    SideMenuComponent,
    SideMenuSearchComponent,
    SideMenuUserComponent,
    SafePipe,
    Safe2Pipe,
    FilterOutStringPipe,
    ClickOutsideDirective
  ],
  imports: [
    ResponsiveModule,
    BrowserModule,
    HttpModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BsModalModule,
    NgxChartsModule,
    MatDatepickerModule,
    MatSelectModule,
    MatNativeDateModule,
    MatRadioModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    NgbModule.forRoot(),
    PerfectScrollbarModule,
    InlineSVGModule
  ],
  providers: [
    DatePickerComponent, 
    MatSnackBarModule, 
    MatSnackBar, 
    WeatherService, 
    TwitterService, 
    TrainsService, 
    ForestItemComponent, 
    JiraForestService, 
    AlertService, 
    FilterService,
    VariablePasserService,
    GetFiltersService,
    SetFiltersService,
    SharedService,
    SvgInlineConverterService,
    JiraParametersService,
    JiraParamSharedService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
