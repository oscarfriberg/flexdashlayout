import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textLength'
})
export class TextLengthPipe implements PipeTransform {

  transform(value: string, maxLength: number): any {
    if (!value) {
        return value;
    }

    let newText = value;

    if (value.length > maxLength) {
        newText = value.substring(0, (maxLength - 3));
        newText += '...';
    }

    return newText;

  }
}
