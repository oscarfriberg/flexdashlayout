import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'subString'
})
export class SubStringPipe implements PipeTransform {

  transform(item: string, startString: string, endString: string): any {
 
    if (!item || !startString || endString) {
      return item;
    }

    else {
      var res = item.substring(item.search(startString) + startString.length, item.search(endString) );
      return res;
    }

  
  }

}
