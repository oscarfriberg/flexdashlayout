import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterOutString'
})
export class FilterOutStringPipe implements PipeTransform {

  transform(item: string, stringToFilterOut: string): any {
 
    if (!item || !stringToFilterOut) {
      return item;
    }

    else {
      var regEx = new RegExp(stringToFilterOut, "ig");
      var stripped = item.replace(regEx, '').replace(/(^[ ]+[-]+[ ]+)/, '');;
      return stripped;
      
    }
  }
}
