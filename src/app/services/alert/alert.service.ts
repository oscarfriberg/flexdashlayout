
import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AlertService {

  constructor(private http: Http) { }

  getAlerts() {
    return this.http.get('http://localhost:3000/api/alert')
    .map(res => res.json());
  }

  postAlert(alert) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/newAlert', JSON.stringify(alert), {headers: headers}).subscribe();
  }

  updateAlert(alert) {

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/api/updateAlert/' + alert._id, JSON.stringify(alert), {headers: headers})
        .map(res => res.json());
  }

  removeAlert(alert) {

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/removeAlert/' + alert._id, JSON.stringify(alert), {headers: headers})
        .map(res => res.json());
  }

}

/*
xfrgkq
localhost:3000
RD0049523
sesoco3319
*/
