import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import axios from 'axios';
@Injectable()
export class TrainsService {

  constructor(private http: Http) { }

  getTrains() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/api/trains/',
        {headers: headers})
            .map(res => res.json());
  }

}
