import { TestBed, inject } from '@angular/core/testing';

import { JiraParamSharedService } from './jira-param-shared.service';

describe('JiraParamSharedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JiraParamSharedService]
    });
  });

  it('should be created', inject([JiraParamSharedService], (service: JiraParamSharedService) => {
    expect(service).toBeTruthy();
  }));
});
