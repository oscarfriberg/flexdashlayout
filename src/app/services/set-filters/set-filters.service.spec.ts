import { TestBed, inject } from '@angular/core/testing';

import { SetFiltersService } from './set-filters.service';

describe('SetFiltersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SetFiltersService]
    });
  });

  it('should be created', inject([SetFiltersService], (service: SetFiltersService) => {
    expect(service).toBeTruthy();
  }));
});
