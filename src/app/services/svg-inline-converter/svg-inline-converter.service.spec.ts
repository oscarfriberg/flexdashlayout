import { TestBed, inject } from '@angular/core/testing';

import { SvgInlineConverterService } from './svg-inline-converter.service';

describe('SvgInlineConverterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SvgInlineConverterService]
    });
  });

  it('should be created', inject([SvgInlineConverterService], (service: SvgInlineConverterService) => {
    expect(service).toBeTruthy();
  }));
});
