import { Injectable } from '@angular/core';

@Injectable()
export class GetFiltersService {

  getStatuses(theObject, field, arrToAddTo) {
    var result = null;
    if(theObject instanceof Array) {
        for(var i = 0; i < theObject.length; i++) {
            result = this.getStatuses(theObject[i], field, arrToAddTo);
        }
    }
    else {
        for(var prop in theObject) {

            if(prop == field) {
              arrToAddTo.push(theObject[prop].name);
            }
            if(theObject[prop] instanceof Object || theObject[prop] instanceof Array)
              result = this.getStatuses(theObject[prop], field, arrToAddTo);
        }
    }
    return result;
  } 

  getNames(arr) {
    var childNames = arr.map(elem => elem.children.map(ch => ch.name));
    return Array.prototype.concat(...childNames);
  }



}
