import { TestBed, inject } from '@angular/core/testing';

import { GetFiltersService } from './get-filters.service';

describe('GetFiltersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetFiltersService]
    });
  });

  it('should be created', inject([GetFiltersService], (service: GetFiltersService) => {
    expect(service).toBeTruthy();
  }));
});
