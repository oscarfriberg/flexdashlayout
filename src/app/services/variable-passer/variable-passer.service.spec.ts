import { TestBed, inject } from '@angular/core/testing';

import { VariablePasserService } from './variable-passer.service';

describe('VariablePasserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VariablePasserService]
    });
  });

  it('should be created', inject([VariablePasserService], (service: VariablePasserService) => {
    expect(service).toBeTruthy();
  }));
});
