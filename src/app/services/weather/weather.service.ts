import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import axios from 'axios';

@Injectable()
export class WeatherService {

  constructor(private http: Http) { }
  
  getWeather() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/api/weather/',
        {headers: headers})
            .map(res => res.json());
  }

}
