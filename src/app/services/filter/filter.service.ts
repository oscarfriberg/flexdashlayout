import { Injectable } from '@angular/core';

import {Http, Headers, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class FilterService {

  constructor() { }

  private subjectFilter = new Subject<any>();

  getFilters(): Observable<any> {
    return this.subjectFilter.asObservable();
  }
  
  sendFilters(filters: any) {
    this.subjectFilter.next(filters);
  }

  clearFilters() {
    this.subjectFilter.next();
  }

}
