import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class JiraParametersService {

  result: any;

  constructor(private __http: Http, private __httpC: HttpClient) { }

  getJiraParams() {
    return this.__http.get('http://localhost:3000/api/jiraParameters')
      .map(result => this.result = result.json().data)
  }

  removeJiraParams(param) {
    console.log('in remove jira param service');
    console.log(param);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.__http.put('http://localhost:3000/api/removeJiraParameter', JSON.stringify(param), {headers: headers})
      .map(result => {
        console.log('service result');
        console.log(result);
        return result;
      })
      .catch(err => {
        return err;
      })
   /*  
   .subscribe( 
      res => { 
        if (res.status == 200){

        }
        else {

        }
      },  
      function(){
          console.log('ok');
        }
    ); 
    */
  }

  addJiraParams(param) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.__http.put('http://localhost:3000/api/newJiraParameter', JSON.stringify(param), {headers: headers}).subscribe();
  }

}
