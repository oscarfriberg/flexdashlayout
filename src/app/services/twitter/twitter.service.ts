import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptionsArgs } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import axios from 'axios';
@Injectable()
export class TwitterService {

  constructor(private http: Http) { }

  getTrump() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/api/twitter/',
        {headers: headers})
            .map(res => res.json());
  }
}
