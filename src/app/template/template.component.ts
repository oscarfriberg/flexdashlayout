import { Component, OnInit, Input } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  @Input()
  tree;

  constructor() { }

  ngOnInit() {
    this.accordionSetup();
  }
  accordionSetup() {
    const headers = ['H1', 'H2', 'H3', 'H4', 'H5', 'H6'];

    $('.accordion').click(function(e) {
        const target = e.target,
            name = target.nodeName.toUpperCase();

        if ($.inArray(name, headers) > -1) {
            const subItem = $(target).next();

            // slideUp all elements (except target) at current depth or greater
            const depth = $(subItem).parents().length;
            const allAtDepth = $('.accordion p, .accordion div').filter(function() {
                if ($(this).parents().length >= depth && this !== subItem.get(0)) {
                    return true;
                }
            });
            $(allAtDepth).slideUp('fast');

            // slideToggle target content and adjust bottom border if necessary
            subItem.slideToggle('fast', function() {
                $('.accordion :visible:last').css('border-radius', '0 0 10px 10px');
            });
            $(target).css({
                'border-bottom-right-radius': '0',
                'border-bottom-left-radius': '0'
            });
        }
    });
  }

}
