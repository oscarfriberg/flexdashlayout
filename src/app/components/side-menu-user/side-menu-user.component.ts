import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbPopoverModule, NgbModule  } from '@ng-bootstrap/ng-bootstrap';
declare var $: any;

@Component({
  selector: 'side-menu-user',
  templateUrl: './side-menu-user.component.html',
  styleUrls: ['./side-menu-user.component.css']
})
export class SideMenuUserComponent implements OnInit {
  
  options: FormGroup;
  chosenFilters = [];
  filters = ['CBM', 'MAC', 'MacCloud', 'MEM', 'MPP'];
  filterControl = new FormControl();

  constructor(fb: FormBuilder) {
    this.options = fb.group({
      hideRequired: false,
      floatLabel: 'auto',
    });
  }

  ngOnInit() {
  }
  playAnim(type){
    console.log('in playAnim side-menu.comp');
    if (type === 'show') {
      $('.sideContent').addClass('active');
    }
    else {
      $('.sideContent').removeClass('active');
    }
  }
  closeSideMenu(id) {
    document.getElementById(id).style.width = '0';  
  }

}
