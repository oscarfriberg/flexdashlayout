import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { TwitterService } from '../../services/twitter/twitter.service';

@Component({
  selector: 'twitter-component',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.css']
})
export class TwitterComponent implements OnInit {
  tweet: any;
  constructor(public twitterService: TwitterService) { }
  ngOnInit() {
    this.getTwitter();
  }


  getTwitter() {

    this.twitterService.getTrump()
        .subscribe(trumpTweet => {
            console.log('trumptweet');
            console.log(trumpTweet);
            this.tweet = trumpTweet;

        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log('client side error occurred (twitter-component)');
            } else {
              console.log('server side error (twitter-component)');
              console.log(err.error);
            }
        });
  }
}
