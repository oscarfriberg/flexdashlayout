import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { TrainsService} from '../../services/trains/trains.service';
@Component({
  selector: 'train-component',
  templateUrl: './trains.component.html',
  styleUrls: ['./trains.component.css']
})
export class TrainsComponent implements OnInit {

  constructor(public trainsService: TrainsService) { }

  trip: any;
  ngOnInit() {
    this.getTrains();
  }

  getTrains() {

    this.trainsService.getTrains()
        .subscribe(tripResponse => {
            console.log(tripResponse);
            this.trip = tripResponse;

        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log('client side error occurred (train-component)');
            } else {
              console.log('server side error (train-component)');
            }
        });
  }

}
