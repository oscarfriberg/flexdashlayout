import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForestItemComponent } from './forest-item.component';

describe('ForestItemComponent', () => {
  let component: ForestItemComponent;
  let fixture: ComponentFixture<ForestItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForestItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForestItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
