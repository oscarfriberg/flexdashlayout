import {HostListener, ElementRef,Component, OnInit, AfterViewInit, Input, OnChanges, ChangeDetectorRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { JiraForestService } from '../../services/jira-forest/jira-forest.service';
import { ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { ForestItemComponent } from '../forest-item/forest-item.component';
import { NgbPopoverModule, NgbModule, NgbPopover  } from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import { VariablePasserService } from '../../services/variable-passer/variable-passer.service';
import { FilterService } from '../../services/filter/filter.service';
import { JiraParamSharedService } from '../../services/jira-param-shared/jira-param-shared.service';


import { GetFiltersService } from '../../services/get-filters/get-filters.service';
import { JiraParametersService } from '../../services/jira-parameters/jira-parameters.service';
import { SharedService } from '../../services/shared-service/shared-service.service';
import { SvgInlineConverterService } from '../../services/svg-inline-converter/svg-inline-converter.service';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

interface Tree {
  name: string;
}
interface FilterType {
  name: string,
  types: Array<string>
}


@Component({
  selector: 'jira-forest',
  templateUrl: './jira-forest.component.html',

  styleUrls: ['./jira-forest.component.css']
})
export class JiraForestComponent implements AfterViewInit {
  
  
  
  testFilter = [
    {name: 'test1',
    types: [1,2]},
    {name: 'test2',
    types: [3,4]}
  ];
  
  
  
  
  
  titel: 'Portfolio';
  trees: Observable<Tree[]>;

  chosenFilters = [];
  filters = [];
  filterControl = new FormControl();
  readyToRender: boolean;
  ts: any;
  
  constructor(
    private cdr: ChangeDetectorRef,
    private jiraForestService: JiraForestService, 
    private forestItem: ForestItemComponent, 
    private variablePasserService: VariablePasserService,
    private filterService: FilterService,
    private getFiltersService: GetFiltersService,
    private sharedService: SharedService,
    private svgService: SvgInlineConverterService,
    private _eref: ElementRef,
    private spinner: MatProgressSpinnerModule,
    private jiraParamService: JiraParametersService,
    private jiraParamSharedService: JiraParamSharedService
  ) {
    this.getJiraParams();
    this.variablePasserService.data.emit('Portfolio');
  }
  // spinner
  color = 'theme';
  mode = 'indeterminate';
 
  forest: any;
  filteredForest: any;
  @Input() outsideClick;
  ngAfterViewInit() {
    this.ts = new Date().getTime();
    this.readyToRender = false;
    this.trees = Observable.of(this.forest);
    this.cdr.detectChanges();
  }

  sendChosenFilters(arr): void {
    // send message to subscribers via observable subject
    this.filterService.sendFilters(arr);
  }

  getJiraParams() {
    this.jiraParamService.getJiraParams()
      .subscribe(res => {
        console.log('structureId: ' + res[0].structureID);
        console.log('structureFolderRegex: ' + res[0].structureFolderRegex);
        console.log('fields:');
        console.log(res[0].fields);
        this.jiraParamSharedService.data.emit(res[0]);
        this.getJiraForest(res[0]);
      });
  }
 
  getJiraForest(params) {

    this.jiraForestService.getJiraForest(params).subscribe(jiraForest => {
      /* console.log('done fetching jira forest');
      console.log('took ' + (new Date().getTime() - this.ts)  + ' ms' ); */
      this.sendChosenFilters(this.filters);
      this.forest = this.filterFolderName(jiraForest);
      this.addFilters(this.forest);
      console.log(this.forest);
      this.readyToRender = true;
      this.filteredForest = JSON.parse(JSON.stringify(this.forest)); 
      this.svgService.convertSVGtoInline();
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          console.log('client side error occurred getJiraForest');
      } else {
          console.log('server side error getJiraForest');
          console.log(err);
        }
      }
    );
  }

/*   getJiraFormulaInfo() {

    this.jiraForestService.getJiraFormulaInfo().subscribe(formulaInfo => {
      console.log(formulaInfo);
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          console.log('client side error occurred (upcoming issues)');
      } else {
          console.log('server side error (upcoming issues)');
        }
      }
    );
  } */

  
  addFilters(arr) {
    
    if (this.filters.findIndex(x => x.name.toLowerCase() === 'bvi') != -1) {
      var bviIndex = this.filters.findIndex(x => x.name.toLowerCase() === 'bvi');
      this.filters[bviIndex].types = this.getFiltersService.getNames(arr);
    }
    else {
      var obj = {
        name: 'bvi',
        types: this.getFiltersService.getNames(arr)
      };
      this.filters.push(obj);
    }
    var apps = {
      name: 'apps',
      types: ['MAC', 'MACC', 'MEM', 'MPP']
    };
    this.filters.push(apps);

    var statuses = [];
    this.getFiltersService.getStatuses(arr, 'status', statuses);
    
    if (this.filters.findIndex(x => x.name.toLowerCase() === 'status') != -1) {
      var statusIndex = this.filters.findIndex(x => x.name.toLowerCase() === 'status');
      this.filters[statusIndex].types = Array.from(new Set(statuses));
    }
    else {
      var obj = {
        name: 'status',
        types: Array.from(new Set(statuses))
      };
      this.filters.push(obj);
    }

    // Sort filters
    this.filters.forEach(group => {
      group.types.sort((a, b) => (a > b ? 1 : -1));
    })

    /* var fixVersions = [];
    this.getFixVersions(arr, 'fixVersions', fixVersions);
    if (this.filters.findIndex(x => x.name.toLowerCase() === 'fix version') != -1){
      var fixVersionIndex = this.filters.findIndex(x => x.name.toLowerCase() === 'fix version');
      this.filters[fixVersionIndex].types = Array.from(new Set(fixVersions));
    }
    else {
      var obj = {
        name: 'fix version',
        types: Array.from(new Set(fixVersions))
      };
      this.filters.push(obj);
    } */
  }

  


  // Körs automatiskt för att få bort irrelevanta folders
  filterFolderName(arr) {
    var reg = new RegExp("^(20[0-9][0-9]) [Qq].*[0-9]");
    this.sharedService.data.emit(reg);
    return arr.filter(item => reg.test(item.name));
  }




  filterForestOnBVI(arr){
    var bvis = this.chosenFilters.find(o => o.name === 'bvi');
    if (!bvis || !bvis.types || bvis.types.length == 0) {
      return arr;
    }

    var res = JSON.parse(JSON.stringify(this.forest));
    res.forEach(folder => {
        folder.children = folder.children.filter(elem => this.chosenFilters.find(o => o.name === 'bvi').types.includes(elem.name));
    });
    return res;
  }



  filterForestOnApp(obj){
    var apps = this.chosenFilters.find(o => o.name === 'apps');
    
    if (!apps || !apps.types || apps.types.length == 0) {
      return obj;
    }

    var tmpobj = obj;
    for (var key in tmpobj) {
      if (obj.fields && obj.fields.project && !apps.types.includes(obj.fields.project.key) && obj.fields.project.key != 'CBM') {
        delete obj[key];
      }
      else if (typeof obj[key] === "object") {
        obj[key] = this.filterForestOnApp(obj[key]);
      }
    }
    return obj;
  }

  filterForestOnStatus(obj){
    var statuses = this.chosenFilters.find(o => o.name === 'status');
    
    if (!statuses || !statuses.types || statuses.types.length == 0) {
      return obj;
    }

    var tmpobj = obj;
    for (var key in tmpobj) {
      if (obj.fields && obj.fields.project && !statuses.types.includes(obj.fields.status.name)) {
        delete obj[key];
      }
      else if (typeof obj[key] === "object") {
        obj[key] = this.filterForestOnStatus(obj[key]);
      }
    }
    return obj;
  }




filterForest() {
  var originalForest = JSON.parse(JSON.stringify(this.forest));
  this.filteredForest = this.filterForestOnBVI(originalForest);
  this.filteredForest = this.filterForestOnApp(this.filteredForest);
  this.filteredForest = this.filterForestOnStatus(this.filteredForest);
}




  // kallas varje gång en checkbox i filtret trycks på
  addRemoveFilter(groupName, type) {
    
    console.log(groupName + ' - ' + type);
    
    // om typen finns..
    const group = this.chosenFilters.find(o => o.name === groupName);
    if (group) {
      if (group.types.includes(type)) {
        group.types.splice(group.types.indexOf(type), 1);
      } else { group.types.push(type); } 
    }
    else {
      var obj = { name: groupName, types: [type] };
      this.chosenFilters.push(obj);
    }

    // om längd är större än 0, skicka med kopia av filtrerade foresten, 
    var numberAppliedFilters = 0;
    this.chosenFilters.forEach(el => {
      numberAppliedFilters += el.types.length;
    });

    console.log('filters:');
    console.log(this.chosenFilters);

    // om inga filters applied, sätt den till originalforesten
    if (numberAppliedFilters == 0) {
      this.filteredForest = JSON.parse(JSON.stringify(this.forest));
    }
    
    // annars filtrera
    else {
      this.filterForest();
    }
  }
}












/* (CBM-3441,
MAC-1476,
MAC-1217,
CBM-3878,
CBM-2766,
CBM-3771,
CBM-3314,
MAC-1471,
MAC-1436,
CBM-3896,
MEM-536,
MEM-583,
MEM-600,
MEM-620,
MAC-1472,
MAC-1411,
MAC-1384,
MPP-1582,
CBM-3393,
MAC-1473,
MAC-1390,
CBM-3669,
MAC-1474,
MAC-1458,
MAC-1449,
MAC-1180,
MAC-1240,
MAC-1448,
MAC-1447,
CBM-3665,
MAC-1486,
MAC-1329,
CBM-3580,
CBM-3873,
CBM-3114,
MAC-1469,
MAC-1470,
MAC-1463,
MAC-1464,
MAC-1465,
MAC-1466,
MAC-1467,
MAC-1206,
MAC-1428,
MAC-1429,
MPP-1580,
MPP-1581,
MPP-1511,
MPP-1559,
CBM-3882,
CBM-3887,
CBM-3938,
MAC-1498,
CBM-3939,
MAC-1499,
MPP-1590,
CBM-3929,
MAC-1500,
CBM-3730,
CBM-3729,
CBM-3880,
CBM-3329,
MAC-1477,
MEM-576,
MPP-1585,
CBM-3927,
CBM-3928,
CBM-3877,
CBM-3502,
CBM-624,
CBM-2626,
CBM-3718,
CBM-3823,
CBM-3884,
CBM-3694,
MEM-604,
MEM-322,
MEM-461,
CBM-3879,
CBM-3437,
CBM-3188,
CBM-3777,
CBM-3829,
CBM-3828,
CBM-3649,
CBM-3881,
CBM-3913,
CBM-3876,
CBM-3595,
CBM-3584,
CBM-3847,
CBM-3584,
CBM-3622,
CBM-3657,
CBM-3888,
CBM-2897,
CBM-3708,
CBM-2946,
CBM-3780,
CBM-3932)&maxResults=200&fields=priority,fixVersions,lastViewed,priority,labels,aggregatetimeoriginalestimate,timeestimate,versions,issuelinks,assignee,status,components,aggregatetimeestimate,creator,subtasks,reporter,aggregateprogress,progress,votes,issuetype,timespent,project,aggregatetimespent,resolutiondate,workratio,watches,created,updated,timeoriginalestimate,description,summary,environment,duedate */












  /* getFixVersions(theObject, field, arrToAddTo) {
    
    var result = null;
    if(theObject instanceof Array) {
        for(var i = 0; i < theObject.length; i++) {
            result = this.getFixVersions(theObject[i], field, arrToAddTo);
        }
    }
    else {
        for(var prop in theObject) {

          if(prop == field) {
            if (theObject[prop].length > 0){
              theObject[prop].forEach(el => {
                arrToAddTo.push(el.name);
              })
            }
          }
          if(theObject[prop] instanceof Object || theObject[prop] instanceof Array)
            result = this.getFixVersions(theObject[prop], field, arrToAddTo);
        }
    }
    return result;
  }  */


/* 
var chosenApps = ['MAC', 'CBM'];
var res = [];
//filterChosenApps(forest);
var arr = [];
//var f = filterChosenApps(forest);
filterChosenApps(forest);

function filterChosenApps(theObject) {
    var result = null;
    if(theObject instanceof Array) {
        for(var i = 0; i < theObject.length; i++) {
            result = this.filterChosenApps(theObject[i]);
        }
    }
    else
    {
		
		if (theObject.fields && !chosenApps.includes(theObject.fields.project.key)) {
			console.log('unmatched object');
			console.log(theObject);
			
		}
		else {
			// console.log('matced object');
			// console.log(theObject);
			// return theObject;
		}
		
        for(var prop in theObject) {
            if(theObject[prop] instanceof Object || theObject[prop] instanceof Array)
                result = this.filterChosenApps(theObject[prop]);
        }
        
    }
    return result;
  }

*/ 



 /* getFilteredObjects(theObject, field, arrToAddTo){

    var result = null;
        if(theObject instanceof Array) {
            for(var i = 0; i < theObject.length; i++) {
                result = this.getFilteredObjects(theObject[i], field, arrToAddTo);
            }
        }
        else {
            for(var prop in theObject) {
                if (!theObject.sort) {
                  arrToAddTo.push(theObject);
                }
                if(prop == 'status' && this.chosenFilters.find(o => o.name === field).types.includes(theObject[prop].name)) {
                    arrToAddTo.push(theObject);
                }
                if(theObject[prop] instanceof Object || theObject[prop] instanceof Array)
                    result = this.getFilteredObjects(theObject[prop], field, arrToAddTo);
            }
        }
        return result;
    } */



    /* 
    
    
var chosenApps = ['MAC', 'CBM'];
var res = [];
var filt = filterChosenApps(forest);

function filterChosenApps(theObject) {
    var result = null;
    if(theObject instanceof Array) {
        for(var i = 0; i < theObject.length; i++) {
            result = this.filterChosenApps(theObject[i]);
        }
    }
    else
    {
		
		if (theObject.fields && !chosenApps.includes(theObject.fields.project.key)) {
			console.log(theObject);	
			return theObject;
		}
		
        for(var prop in theObject) {
            if(theObject[prop] instanceof Object || theObject[prop] instanceof Array)
                result = this.filterChosenApps(theObject[prop]);
        }
        
    }
    return result;
  }
    
    */























/* 
var forest = [
  {
    name: 'qw', children: [
      {
        name: 'we',
        fields: {
          status: {name: 'New' },fixVersions: []
        }
      },
      {
        name: 'er',
        fields: {
          status: {name: 'Old' },
			fixVersions: []
        }
      },
      {
        name: 'rt',
        fields: {
          status: {name: 'Old' },
		fixVersions: [
            {name: 'fixV1'}
		]
			
        },
        children: [
          {
            name: 'bn'
          }
        ]
      }
    ]
  },
  {
    name: 'zx', children: [
      {
        name: 'xc',
        fields: {
          status: {name: 'SuperNew' },
		fixVersions: [
            {name: 'fixV1'},
			{name: 'fixV2'}
        ]
        }
      },
      {
        name: 'cv'
      },
      {
        name: 'vb',
        fields: {
          status: {name: 'Old' },
			fixVersions: [
            {name: 'fixV3'}
		]
        }
      }
    ]
  }
];

var statuses = [];
getStatuses(forest, 'status', statuses);
var uniqueStatuses = Array.from(new Set(statuses));

var fixVersions = [];
getFixVersions(forest, 'fixVersions', fixVersions);
var uniqueFixVersions = Array.from(new Set(fixVersions));

// console.log('Statuses:');
// uniqueStatuses;

console.log('fixversions');
console.log(uniqueFixVersions);


function getStatuses(theObject, field, arrToAddTo) {
    var result = null;
    if(theObject instanceof Array) {
        for(var i = 0; i < theObject.length; i++) {
            result = this.getStatuses(theObject[i], field, arrToAddTo);
        }
    }
    else
    {
        for(var prop in theObject) {

            if(prop == field) {
				console.log('found status: ' + theObject[prop].name);
              	arrToAddTo.push(theObject[prop].name);
            }
            if(theObject[prop] instanceof Object || theObject[prop] instanceof Array)
                result = this.getStatuses(theObject[prop], field, arrToAddTo);
        }
    }
    return result;
  }

function getFixVersions(theObject, field, arrToAddTo) {
  var result = null;
  if(theObject instanceof Array) {
      for(var i = 0; i < theObject.length; i++) {
          result = this.getFixVersions(theObject[i], field, arrToAddTo);
      }
  }
  else
  {
      for(var prop in theObject) {

          if(prop == field) {
        if (theObject[prop].length > 0){
          theObject[prop].forEach(el => {
            console.log(el);
			arrToAddTo.push(el.name);
          })
        }
        
           // arrToAddTo.push(theObject[prop]);
          }
          if(theObject[prop] instanceof Object || theObject[prop] instanceof Array)
              result = this.getFixVersions(theObject[prop], field, arrToAddTo);
      }
  }
  return result;
} 




function getFilteredObjects(theObject, field, arrToAddTo){
var result = null;
    if(theObject instanceof Array) {
        for(var i = 0; i < theObject.length; i++) {
            result = this.getFilteredObjects(theObject[i], field, arrToAddTo);
        }
    }
    else
    {
        for(var prop in theObject) {

            if(prop == 'status' && chosenStatuses.includes(theObject[prop].name)) {
			
              	arrToAddTo.push(theObject);
            }
            if(theObject[prop] instanceof Object || theObject[prop] instanceof Array)
                result = this.getFilteredObjects(theObject[prop], field, arrToAddTo);
        }
    }
    return result;

}

var b = [];
getFilteredObjects(forest, 'status', b);
b;
*/

 /*  @Input() set fullForest(value: any) {
    this._fullForest = value;
    // do something on 'aa' change
    console.log('changed fullforest=');
    console.log(this.fullForest);
  }
 */
/*   @Input() fullForest: any;
ngOnChanges(fullForest: any) {
    console.log('changed forest?');
} */

 /* this.filt = Observable.of(this.filters);
    this.filtC = Observable.of(this.filterControl); */

      /* chosenFilters: any[]; */
 /*  filt: Observable<any>;
  filtC: Observable<any>; */
