import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared-service/shared-service.service';
import { JiraParamSharedService } from '../../services/jira-param-shared/jira-param-shared.service';
import { JiraParametersService } from '../../services/jira-parameters/jira-parameters.service';
declare var $: any;
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';

import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  form: FormGroup;
  private formSubmitAttempt: boolean;

  myData: any;

  jiraParams: any;
  isCollapsed = true;

  options: FormGroup;
  chosenFilters = [];
  filters = ['Portfolio'];
  filterControl = new FormControl();

  constructor(
    private sharedService: SharedService,
    private jiraParamSharedService: JiraParamSharedService,
    private jiraParametersService: JiraParametersService,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
    fb: FormBuilder
    ) {
      this.options = fb.group({
        hideRequired: false,
        floatLabel: 'auto',
      });
    this.sharedService.data.subscribe(
      (data: any) => {
        console.log(data);
        this.myData = data;
    });
    this.jiraParamSharedService.data.subscribe(
      (data: any) => {
        console.log('jira params in sidemenu');
        console.log(data);
        this.jiraParams = data;
    });
  }
  newField: string;
  ngOnInit() {
    this.form = this.formBuilder.group({
      field: [null, Validators.required],
      structureID: [null, Validators.required],
      structureFolderRegex:[null, Validators.required]
    });
   }

  postJiraParam() {
    if (this.form.value.field) {
      console.log(this.form.value.field);
      this.openSnackBar('Posted New Jira Param');
      this.jiraParametersService.addJiraParams(this.form.value);
      this.reset();
    }
  }
  removeJiraParam(inField) {
    console.log('sending field ' + inField + ' for removal');
    this.openSnackBar('Removed Jira Param' + inField);
    var obj = {
      field: inField
    }
    this.jiraParametersService.removeJiraParams(obj).subscribe(async res => {
      console.log('done?');
      console.log(res);
      // if (res.status == 200
      this.jiraParametersService.getJiraParams();
      
    });
  }

  reset() {
    this.form.reset();
    this.formSubmitAttempt = false;
  }

  openSnackBar(message) {
    this.snackBar.open(message, 'OK', {
        duration: 4000,
    });
  }

  playAnim(type){
    console.log('in playAnim side-menu.comp');
    if (type === 'show') {
      $('.sideContent').addClass('active');
    }
    else {
      $('.sideContent').removeClass('active');
    }
  }
  closeSideMenu(id) {
    document.getElementById(id).style.width = '0';  
  }

}
