import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import {HttpClientModule, HttpClient} from '@angular/common/http';

import { AlertService } from '../../services/alert/alert.service';
import { JiraForestService } from '../../services/jira-forest/jira-forest.service';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    AlertService,
    JiraForestService
  ]
})
export class AppComponent {
  title = 'app';
  
  playAnim(type){
    console.log('in playAnim app.comp');
    if (type === 'show') {
      console.log('type: ' + type);
      $('.sideContent').addClass('active');
    }
    else {
      console.log('type: not ');
      $('.sideContent').removeClass('active');
    }
  }

  openSideMenu(id) {
    document.getElementById(id).style.width = '380px';
  }

 closeSideMenu(id) {
    document.getElementById(id).style.width = '0';  
  }
  
}
