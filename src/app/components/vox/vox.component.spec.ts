import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoxComponent } from './vox.component';

describe('VoxComponent', () => {
  let component: VoxComponent;
  let fixture: ComponentFixture<VoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
