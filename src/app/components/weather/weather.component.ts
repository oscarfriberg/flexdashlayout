import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, RequestOptions  } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { WeatherService } from '../../services/weather/weather.service';


@Component({
  selector: 'weather-component',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  weather: any;

  
  constructor(public weatherService: WeatherService) { }

  ngOnInit() {
    this.getWeather();
  }


  getWeather() {

    this.weatherService.getWeather()
        .subscribe(weatherResponse => {
            console.log(weatherResponse);
            this.weather = weatherResponse;

        },
        (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log('client side error occurred (weather-component)');
            } else {
              console.log('server side error (weather-component)');
            }
        });
  }

}
