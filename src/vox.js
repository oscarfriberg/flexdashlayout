var final_transcript = '';
var recognizing = false;
var playingBot = false;
const colors = [
    'black', 'white', 'green', 'blue', 'red'
]


if ('webkitSpeechRecognition' in window) {
    console.log('listening');

    var voices = window.speechSynthesis.getVoices();
    // för att den ska "hinna" hämta alla voices, blir alltid voice[0] första gången annars
    window.speechSynthesis.onvoiceschanged = function() {
        voices = window.speechSynthesis.getVoices();
    };

    var recognition = new webkitSpeechRecognition();

    recognition.continuous = true;
    recognition.interimResults = true;

    recognition.onstart = function() {
        document.getElementById('readyForSpeech').innerHTML = 'Ready...';
        recognizing = true;
    };

    recognition.onerror = function(event) {
        document.getElementById('readyForSpeech').innerHTML = event.error;
    };

    recognition.onend = function() {
        recognizing = false;
    };

    recognition.onresult = function(event) {

        console.log('on result');
        var interim_transcript = '';
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
                const phrase = event.results[i][0].transcript;
                const chosenColor = colors.filter(col => phrase.toLowerCase().includes(col));
                if (chosenColor.length > 0) {
                    speekChange(chosenColor);
                }
                if (phrase.toLowerCase().includes('trump')) {
                    readTweet();
                }
                console.log('is final');
                console.log(event.results[i][0].transcript);
                final_transcript = event.results[i][0].transcript;
                final_transcript = capitalize(final_transcript);
                final_span.innerHTML = linebreak(final_transcript);
            } else {
                interim_transcript += event.results[i][0].transcript;
                console.log('interim_transcript');
                console.log(interim_transcript);
                final_span.innerHTML = interim_transcript;
            }
        }

        // interim_span.innerHTML = linebreak(interim_transcript);
    };
}

function playSFX(color) {
    console.log('in playsfx');
    var grunts = [
        'http://www.freesfx.co.uk/rx2/mp3s/4/16643_1460665407.mp3',
        'http://www.freesfx.co.uk/rx2/mp3s/4/16641_1460665406.mp3',
        'http://www.freesfx.co.uk/rx2/mp3s/4/16642_1460665406.mp3',
        'http://www.freesfx.co.uk/rx2/mp3s/4/16644_1460665602.mp3'
    ];
    var gruntIndex = Math.floor(Math.random() * grunts.length);
    var audio = new Audio(grunts[gruntIndex]);
    audio.onended = function() {
        console.log(1);
        console.log('done');
        document.getElementById('readyForSpeech').innerHTML = 'Ready...';
        recognizing = true;
        recognition.start();
        document.getElementById("interim_span").style.color = color;
        document.getElementById("final_span").style.color = color;

    }
    audio.play();
}

function readTweet() {
    recognizing = false;
    recognition.stop();
    console.log('in read trump');
    var msg = new SpeechSynthesisUtterance();
    // var voices = window.speechSynthesis.getVoices();
    msg.voice = voices[3]; // Note: some voices don't support altering params
    msg.voiceURI = 'native';
    msg.volume = 1; // 0 to 1
    msg.rate = 0.8; // 0.1 to 10
    msg.pitch = 0.001; //0 to 2
    var fullTweet = document.getElementById('trumpTweet').textContent;
    var tweetSplit = fullTweet.split(' ');

    var strippedString = '';

    tweetSplit.forEach(elem => {
        if (!elem.includes('http')) {
            strippedString += elem + ' ';
        }
    })
    msg.text = strippedString;
    console.log(msg.text);
    msg.lang = 'en-US';
    msg.onend = function(e) {
        recognizing = true;
        recognition.start();
        console.log('stopped read trump');
    };
    speechSynthesis.speak(msg);

}

function speekChange(color, utterance) {
    recognizing = false;
    recognition.stop();
    document.getElementById('readyForSpeech').innerHTML = 'Processing... processing...';
    var msg = new SpeechSynthesisUtterance();
    // var voices = window.speechSynthesis.getVoices();
    msg.voice = voices[17]; // Note: some voices don't support altering params
    msg.voiceURI = 'native';
    msg.volume = 1; // 0 to 1
    msg.rate = 0.8; // 0.1 to 10
    msg.pitch = 0.001; //0 to 2
    msg.text = 'changing color to ' + color;
    msg.lang = 'en-US';
    msg.onend = function(e) {
        playSFX(color);
    };
    speechSynthesis.speak(msg);
    console.log(23);

}
var two_line = /\n\n/g;
var one_line = /\n/g;

function linebreak(s) {
    return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}

function capitalize(s) {
    return s.replace(s.substr(0, 1), function(m) {
        return m.toUpperCase();
    });
}

function startDictation(event) {
    console.log('start dic');
    if (recognizing) {

        recognition.stop();
        return;
    }
    final_transcript = '';
    recognition.lang = 'en-US';
    recognition.start();
    final_span.innerHTML = '';
    interim_span.innerHTML = '';

}