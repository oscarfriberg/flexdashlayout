const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;

const dbURL_local = "mongodb://localhost:27017/";
const dbName_local = "flexDB";

const jiraParams = "jiraParameters";


// Connect 
const connection = (closure) => {
    return MongoClient.connect(dbURL_local + dbName_local, (err, db) => {
        if (err) return console.log(err);
        closure(db);
    });
};

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};


// Get parameters
router.get('/jiraParameters', (req, res) => {
    connection((db) => {
        const dataBase = db.db(dbName_local);
        var collection = dataBase.collection(jiraParams);
        collection
            .find()
            .toArray()
            .then((jiraparams) => {
                response.data = jiraparams;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

// Remove parameters
router.put('/removeJiraParameter', (req, res, next) => {
    console.log('in remove parameter');
    var entry = req.body;
    console.log('removal entry');
    console.log(entry);
    connection((db) => {
        const dataBase = db.db(dbName_local);
        var collection = dataBase.collection(jiraParams);
        
        collection
            .update({gridItemType: "Portfolio"}, { $pull: { fields: entry.field } })
            .then((jiraparams) => {
                response.data = jiraparams;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    }); 
});

// Post New  param
router.put('/newJiraParameter', (req, res, next) => {
    // db.jiraParameters.update({name: 'asdf'}, {"$set":{gridItemType:"asdf"}}, {upsert: true});
    console.log('in new jira parameter');
    var entry = req.body;
    console.log(entry);

    var objForUpdate = {};

    if (entry.field) objForUpdate.field = entry.field;
    if (entry.structureID) objForUpdate.structureID = entry.structureID;
    if (entry.structureFolderRegex) objForUpdate.structureFolderRegex = entry.structureFolderRegex;

    var upd;
    if (entry.structureFolderRegex || entry.structureID) {
        upd = { $set: objForUpdate };
    }
    else if (entry.field) {
        upd = {$addToSet: { fields: entry.field } };
    }

    connection((db) => {
        const dataBase = db.db(dbName_local);
        var collection = dataBase.collection(jiraParams);
        collection
            .update( {gridItemType: 'Portfolio'}, upd, {upsert: true})
            .then((jiraparams) => {
                response.data = jiraparams;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

module.exports = router;

 