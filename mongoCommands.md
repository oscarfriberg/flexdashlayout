    1. Ladda ner och installera NodeJS
    2. Öppna cmd och navigera till projektmappen
    3. npm install (node package manager - installerades tillsammans med nodejs)
        - har nu skapat mappen node_modules i projektet
    4. npm run build för att builda - låt den run'a så uppdateras den själv vid ändring
        av typescript-filerna
    5. Öppna nytt cmd-window och i foldern skriv 'npm start' som startar
        node-servern (denna har backenden och npm run build frontenden)
        - kan nu localhosta
        - stäng ner med CTRL + C (starta om vid ändring av backendkod)
    6. Install mongodb (community server)
        - server -> mongod
        - client -> mongo
    7. Starta servern (mongod) och låt stå på medan du jobbar med projektet 
        (försök att nå den medan kommer krascha projektet) 
    8. Starta shell clienten (mongo) för att komma åt db.
        - use db node-angular (db uppsatt i kurs)
        - show collections
        - db.messages.find() eller db.users.find()
        - shell kommer inte användas så mkt, node fixar den biten
    9. Öppna projektet och dra upp en terminal och skriv:
        - npm install --save mongoose (--save -> save as a production dependency i packages.json filen)
    10. Installera mongoose validator för att kunna använda validering av 
        "unique" i modeller (primary key typ)
        - npm install --save mongoose-unique-validator
    11. 

    I projekt för att starta:
    CTRL + ö för terminal
    npm install
    npm install --save mongoose
    npm install --save mongoose-unique-validator
    Terminal: C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe


    How to use:

    1) Navigate into the folder (in the command line/ terminal)

    2) Run "npm install" to install all required dependencies

    3) Run "npm run build" to run the development build script => Keep this process running! It recompiles your files upon changes.

    4) Run "npm start" in a new command line/ terminal window => Keep this process running as well, it's your NodeJS server. 


    set up new user in mongo.exe
    use [databas]

    db.createUser({ 
        user: "<name>",
        pwd: "<cleartext password>",
        customData: { <any information> },
        roles: [
            { role: "<role>", db: "<database>" } | "<role>",
        ...
    ]
    })

    db.createUser({ 
        user: "flexUser",
        pwd: "flexPass",
        roles: [
            { role: "readWrite", db: "testDatabas" }, "dbAdmin",
    ]
    })

    db.jiraIDS.insert(
        {
            structureID: 371, 
            structureFolderRegex:"^(20[0-9][0-9]) [Qq].*[0-9]",
            fields: [
                'priority',
                'fixVersions',
                'lastViewed',
                'priority',
                'labels',
                'aggregatetimeoriginalestimate',
                'timeestimate',
                'versions',
                'issuelinks',
                'assignee',
                'status',
                'components',
                'aggregatetimeestimate',
                'creator',
                'subtasks',
                'reporter',
                'aggregateprogress',
                'progress',
                'votes',
                'issuetype',
                'timespent',
                'project',
                'aggregatetimespent',
                'project',
                'aggregatetimespent',
                'resolutiondate',
                'workratio',
                'watches',
                'created',
                'updated',
                'timeoriginalestimate',
                'description',
                'summary',
                'environment',
                'duedate'
            ]
        });


    db.createCollection('customers');
    show collections;
    db.customers.insert({first_name:"John", last_name:"Doe"});
    db.customers.find();
    db.customers.insert([ {first_name : "Adam", last_name : "Smith"}, {first_name : "Eve", last_name : "Smith", gender : "Female"} ]);
    db.customers.find().pretty();
    db.customers.update({first_name:"John"}, {first_name:"John", last_name:"Doe", gender:"Male"});
    db.customers.update({first_name:"Adam"}, {$set:{gender:"Male"}});
    db.customers.update({first_name:"Adam"}, {$set:{age:45}});
    db.customers.update({first_name:"Adam"}, {$inc:{age:5}});
    db.customers.update({first_name:"Adam"}, {$unset:{age:1}});
    db.customers.update({first_name:"Mary"}, {first_name:"Mary", last_name:"Doe"}, {upsert: true});
    db.customers.update({first_name:"Adam"}, {$rename:{"gender":"sex"}});
    db.customers.remove({first_name:"Adam"}, {justOne:true});
    db.supportCases.update({_id:ObjectId("59cba98bcfeb68b518a07618")}, {number:4});
    db.supportCases.update({_id:ObjectId("59cba98bcfeb68b518a07618")}, {$set:{supportType:"Closed"}});

    db.customers.insert(
        {
            first_name:"Doan", 
            last_name:"Joes",
            address:{
                street:"StreetOne 1",
                city: "City1",
                country: "CountryOne"
            },
            memberships:["mem1", "mem2"],
            balance: 123.32
        });

    db.customers.find({first_name:"Mary"}).pretty();

    db.customers.find({$or:[{first_name:"Mary"}, {first_name:"Adam"}]}).pretty();

    db.customers.find({age:{$gt:40}});

    db.customers.find({"address.city":"City1"});

    db.customers.find({memberships:"City1"});

    db.customers.find().sort({last_name:1}); // asc

    db.customers.find().sort({last_name:1}); // asc







    db.slack.find({goMessagePreTimeStamp});


    db.slack.insert({goMessagePreTimeStamp: "Howdy, We do have a decision from the GO/ No GO Meeting today at ", goMessagePostTimeStamp:" and the decision is a GO.  With regards / PO Team"});
    db.slack.insert({nogoMessagePreTimeStamp: "Howdy, We do have a decision from the GO/ No GO Meeting today at ", nogoMessagePostTimeStamp:" and the decision is a NO GO.  With regards / PO Team"});

    db.slack.insert({sickAbsceneMessage: "is ill/ absent as from"});
    db.slack.insert({workHomeMessage: "will work at home today"});
    db.slack.find({},{sickAbsceneMessage}).pretty();

    db.supportCases.find({supportType: ''});
    db.slack.find({messageType: "goMessage"});

    db.slack.insert(
        {
            messageType:"goMessage", 
            message:{
                preTimeStamp:"Howdy, We do have a decision from the GO/ No GO Meeting today at ",
                postTimeStamp: " and the decision is a GO.  With regards / PO Team"
            }
        },
        {
            messageType:"nogoMessage", 
            message:{
                preTimeStamp:"Howdy, We do have a decision from the GO/ No GO Meeting today at ",
                postTimeStamp: " and the decision is a NO GO.  With regards / PO Team"
            }
        },
        {
            messageType:"sickAbsceneMessage", 
            message:{
                afterNameBeforeTime:"is ill/ absent as from"

            }
        },{
            messageType:"workHomeMessage", 
            message:{
                afterName:"will work at home today"
            }
        });









    db.createCollection('slackTemplates');
    db.createCollection('supportCases');

    db.slackTemplates.insert({"messageTemplate": "Howdy! After the meeting at <TIME> we had agreed to a Go! /PO-team.",
        "messageType": "goMessage"});

        db.slackTemplates.insert({"messageTemplate": "Hey! After meeting at <TIME> we have decided for a No Go! /PO-team",
        "messageType": "noGoMessage"});

        db.slackTemplates.insert({"messageTemplate": "<NAME> is ill today.",
        "messageType": "abscenceMessage"});

        db.slackTemplates.insert({"messageTemplate": "<NAME> is working frffom home today.",
        "messageType": "workFromHomeMessage"});

    db.supportCases.insert({"number": "43",
    "supportType": "Open"});

    db.supportCases.insert({"number": "12",
        "supportType": "Closed"});

    db.supportCases.insert({"number": "32",
        "supportType": "Restored"});

    db.supportCases.insert({"number": "42",
        "supportType": "New"});


        var startDate = new Date();
        var endDate = new Date(startDate);
        endDate.setDate(endDate.getDate() + 2);

        db.Alerts.update({"title": "Title2"}, {"$set":{"startDate": startDate}});
        db.Alerts.update({"title": "Title2"}, {"$set":{"endDate": endDate}});

        Update field name:
        db.Collection.update({}, {$rename:{"name.additional":"name.last"}}, false, true);
        db.Alerts.update({}, {$rename:{"startDate":"publishDate"}}, false, true);

        Remove fieldnames:
        db.Alerts.update({}, {$unset: {creationDate:1}} , {multi: true});

        db.Alerts.remove({message:"123"}, {multi: true});
